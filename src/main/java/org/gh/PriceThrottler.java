package org.gh;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

/**
 * TODO: Add support for multiple ccy pairs, publishers and listeners.
 * We are implementing a solution for one publisher and one
 * listener only. We also ignore ccy pair (assuming that it is the same
 * pair throughout the program lifetime.
 * Probably this can be implemented on top of ConcurrentHashMap where
 * keys would be ccy pairs and values -- rates (or pairs "ccy pair/rate").
 * This way we could store the latest rates for each ccy pair which would
 * be enough to provide multiple subscribers with the latest data.
 * Probably we'll need a copy of this structure for each listener to track
 * when a listener has finished its job.
 * Probably ConcurrentSkipListMap would fit better in case each listener
 * can handle multiple ccy pairs because we can order rates being added by
 * their insertion time.
 * Probably a rejection handler of an executor can be used to achieve the goal.
 */
public class PriceThrottler implements PriceListener {

    public static final CcyRate DUMMY = new CcyRate("DUMMY", 0.0);
    private final ConcurrentHashMap<String, LinkedHashMap<String, CcyRate>> latestRates = new ConcurrentHashMap<>();
    private final List<Thread> listenerThreads = new ArrayList<>();

    private static class CcyRate {
        private final String ccyPair;
        private final double rate;

        public CcyRate(String ccyPair, double rate) {
            this.ccyPair = ccyPair;
            this.rate = rate;
        }

        public String getCcyPair() {
            return ccyPair;
        }

        public double getRate() {
            return rate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CcyRate ccyRate = (CcyRate) o;

            if (!ccyPair.equals(ccyRate.ccyPair)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return ccyPair.hashCode();
        }
    }

    public void addListener(final String name, final PriceListener listener) {
        latestRates.put(name, new LinkedHashMap<>());
        Thread listenerThread = new Thread(() -> {
            //TODO: add a unit test for interruption
            while (!Thread.currentThread().isInterrupted()) {
                CcyRate latestRateCopy = DUMMY;
                LinkedHashMap<String, CcyRate> ratesQueue = latestRates.get(name);
                //TODO: replace busy wait with condition queues
                synchronized (ratesQueue) {
                    Iterator<CcyRate> iterator = ratesQueue.values().iterator();
                    if (iterator.hasNext()) {
                        latestRateCopy = iterator.next();
                        iterator.remove();
                    }
                }
                if (latestRateCopy == DUMMY) {
                    continue;
                }
                listener.onPrice(latestRateCopy.getCcyPair(), latestRateCopy.getRate());
                System.out.println("listener returned to throttler " + latestRateCopy.getRate() + " latRate: " + latestRateCopy.getRate());
                System.out.println("listener nulled rate " + latestRateCopy.getRate());
            }
        });
        listenerThreads.add(listenerThread);
        listenerThread.start();
    }

    @Override
    public void onPrice(String ccyPair, double rate) {
        latestRates.forEach((listener, ratesQueue) -> {
            CcyRate ccyRate = new CcyRate(ccyPair, rate);
            synchronized (ratesQueue) {
                ratesQueue.put(ccyPair, ccyRate);
            }
        });
    }

    public void stop() {
        for (Thread listenerThread : listenerThreads) {
            listenerThread.interrupt();
        }
    }
}
