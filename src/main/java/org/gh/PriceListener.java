package org.gh;

/**
 * Created by dev on 31.03.15.
 */
public interface PriceListener {
    void onPrice(String ccyPair, double rate);
}
