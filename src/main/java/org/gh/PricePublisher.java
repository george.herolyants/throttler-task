package org.gh;

/**
 * Created by dev on 31.03.15.
 */
public abstract class PricePublisher implements Runnable {
    private final String ccyPair;
    private PriceListener listener;

    public PricePublisher(String ccyPair, PriceListener listener) {
        this.ccyPair = ccyPair;
        this.listener = listener;
    }

    public void run() {
        while (isRunning()) {
            listener.onPrice(ccyPair, produce());
            sleep(delay());
        }
    }

    private void sleep(int delayInMillis) {
        try {
            Thread.sleep(delayInMillis);
        } catch (InterruptedException e) {
            //TODO: add proper handling of thread interruption
            e.printStackTrace();
        }
    }

    protected abstract double produce();

    protected abstract int delay();

    protected abstract boolean isRunning();

}
