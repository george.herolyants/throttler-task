package org.gh;

import java.util.Arrays;

/**
 * Created by dev on 02.04.15.
 */
public class Tests {
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            System.out.println("Interrupted " + Thread.currentThread().getName());
            Thread.currentThread().interrupt();
        }
    }

    public static <T> T[] $(T... rate) {
        return rate;
    }

    public static <T> boolean in(T[] collection, T item) {
        return Arrays.binarySearch(collection, item) >= 0;
    }
}
