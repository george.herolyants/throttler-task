package org.gh;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PricePublisherTest {

    private List<Double> result = new ArrayList<>();
    private int counter;

    @Test
    public void testProduce() throws InterruptedException {
        PriceListener listener = (ccyPair, rate) -> result.add(rate);

        PricePublisher publisher = new PricePublisher("USD/EUR", listener) {

            @Override
            protected double produce() {
                return ++counter;
            }

            @Override
            protected int delay() {
                return 1;
            }

            @Override
            protected boolean isRunning() {
                return counter < 4;
            }
        };

        Thread thread = new Thread(publisher);
        thread.start();
        thread.join();

        assertThat(result, Matchers.contains(1.0, 2.0, 3.0, 4.0));
    }

}