package org.gh;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.gh.Tests.sleep;
import static org.hamcrest.Matchers.contains;

/**
 * Created by dev on 31.03.15.
 */
public class PriceThrottler1Publisher1Listener1CcyPairTest {

    public static final int TIME_TO_CATCH_UP = 1000;
    private List<Double> delivered = new ArrayList<>();
    private int counter;
    private double[] original = {1.2, 10.0, 20.0, 2.5, 11.0, 22.0, 0.3};

    @Test
    public void testSkippingRates() throws InterruptedException {

        PriceThrottler throttler = new PriceThrottler();

        throttler.addListener("listener0", (ccyPair, rate) -> {
            delivered.add(rate);
            sleep(TIME_TO_CATCH_UP);
        });

        PricePublisher producer = new PricePublisher("CHF/USD", throttler) {
            @Override
            protected double produce() {
                return original[counter++];
            }

            @Override
            protected int delay() {
                //delay() is called after produce(). So when counter == 4 it's the 3rd element in the
                //array being processed at the moment. Same for 7 -- it's the last element.
                //TODO: try to make this behaviour more explicit
                if (counter == 4 || counter == 7) {
                    return TIME_TO_CATCH_UP;
                }
                return 10;
            }

            @Override
            protected boolean isRunning() {
                return counter < original.length;
            }
        };

        Thread thread = new Thread(producer);
        thread.start();
        thread.join();

        sleep(1000);
        throttler.stop();

        Assert.assertThat(delivered, contains(1.2, 2.5, 0.3));
    }
}
