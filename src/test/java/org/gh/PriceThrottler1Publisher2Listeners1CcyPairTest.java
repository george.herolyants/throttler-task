package org.gh;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.gh.Tests.$;
import static org.gh.Tests.in;
import static org.gh.Tests.sleep;
import static org.hamcrest.Matchers.contains;

/**
 * Created by dev on 31.03.15.
 */
public class PriceThrottler1Publisher2Listeners1CcyPairTest {

    private List<Double> delivered1 = new ArrayList<>();
    private List<Double> delivered2 = new ArrayList<>();
    private AtomicInteger counter = new AtomicInteger(-1);
    private double[] original = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};

    private AtomicInteger listenersWaiting = new AtomicInteger(0);
    private AtomicInteger listenersCompleted = new AtomicInteger(2);

    @Test
    public void testSkippingRates() throws InterruptedException {
        PriceThrottler throttler = new PriceThrottler();

        throttler.addListener("listener1", (ccyPair, rate) -> {
            delivered1.add(rate);
            listen(rate, "listener1", $(3), 2);
            if (counter.get() >= original.length) {
                sleep(1000L);
            }
            System.out.println("listener1 completed with return: " + rate);
        });
        throttler.addListener("listener2", (ccyPair, rate) -> {
            delivered2.add(rate);
            listen(rate, "listener2", $(2, 6), 1);
            System.out.println("listener2 completed with return: " + rate);
        });

        PricePublisher producer = new PricePublisher("CHF/USD", throttler) {
            @Override
            protected double produce() {
                double rate = original[counter.incrementAndGet()];
                System.out.println("producer cnt: " + counter + " rate:" + rate);
                return rate;
            }

            @Override
            protected int delay() {
                while (!listenersCompleted.compareAndSet(2, 0)) {
                    sleep(1L);
                }
                System.out.println("producer delay cnt: " + counter);
                listenersWaiting.set(2);
                return 10;
            }

            @Override
            protected boolean isRunning() {
                return counter.get() + 1 < original.length;
            }
        };

        Thread thread = new Thread(producer);
        thread.start();
        thread.join();

        counter.incrementAndGet();

        sleep(2000L);

        throttler.stop();

        Assert.assertThat(delivered2, contains(1.0, 3.0, 7.0));
        Assert.assertThat(delivered1, contains(1.0, 4.0, 7.0));
    }

    private void listen(double rate, String listenerName, Integer[] returnOnCounterValues, int listenerOrder) {
        System.out.println(listenerName + " onPrice called " + counter.get());
        outer: while (true) {
            System.out.println(listenerName + " entered loop " + counter.get());
            do {
                if (counter.get() >= original.length) {
                    break outer;
                }
                sleep(1L);
            } while (!listenersWaiting.compareAndSet(listenerOrder, listenerOrder - 1));
            int localCounter = counter.get();
            double currentRate = original[localCounter];
            System.out.println(listenerName + " cnt: " + localCounter + " curRate: " + currentRate + " rate: " + rate);
            if (in(returnOnCounterValues, localCounter)) {
                listenersCompleted.incrementAndGet();
                break;
            }
            listenersCompleted.incrementAndGet();
            System.out.println(listenerName + " skipped: " + currentRate);
        }
    }

}
